from django.http import HttpResponse
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage, default_storage
from k_nearest import worker
import operator as op
import pdb

def append_path(image):
    image = image[1]
    if 'data/' not in image.path:
        image.path = 'data/' + image.path
        image.label = image.label[10:]
    return image

def get_saved_file(prefix, filepath):
    fs = FileSystemStorage(location=prefix)
    filename = fs.save(filepath.name, filepath)
    return fs.url(filename)


def index(request):
    context = {}
    if request.method == 'POST' and request.FILES:
        prefix = 'caffeimages/static/data/'
        filepath = request.FILES['file_path']
        uploaded_file_url = get_saved_file(prefix, filepath)
        n_results = int(request.POST['spin_number'])
        images, labeled_image = worker.get_top(prefix+uploaded_file_url, n_results, worker.n, worker.fast_array)
        images = map(append_path, images)

        context = {
            'uploaded_file_url': uploaded_file_url ,
            'image' : filepath,
            'label' : labeled_image.label[10:],
            'number_of_results' : n_results,
            'updt_img' : "/static/data/" + uploaded_file_url,
            'img_list' : images
        }
    return render(request, 'caffeimages/base.html', context)
